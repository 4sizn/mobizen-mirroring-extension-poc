// tiny wrapper with default env vars
module.exports = {
  NODE_ENV: (process.env.NODE_ENV || 'development'),
  PORT: (process.env.PORT || 3000),
  // MOBIZEN_URL: (process.env.MOBIZEN_URL || 'http://172.25.110.181:6001'),
  // PUSH_SERVER_URL: (process.env.PUSH_SERVER_URL || 'http://127.0.0.1:3000')
  MOBIZEN_URL: (process.env.MOBIZEN_URL || 'https://st.mobizen.com/'),
  PUSH_SERVER_URL: (process.env.PUSH_SERVER_URL || 'http://172.25.110.181:3000')
  // URL: (process.env.URL || 'http://172.25.110,181')
}
