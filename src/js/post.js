/* chrome extension not allow new tab or new window func
  in html target Attribute _blank so use this code trick to open new page
  & send to data by post method */

var onMessageHandler = function (message) {
  chrome.runtime.onMessage.removeListener(onMessageHandler)

  var form = document.createElement('form')
  form.setAttribute('method', 'post')
  form.setAttribute('action', message.url)
  for (var key in message.data) {
    var hiddenField = document.createElement('input')
    hiddenField.setAttribute('type', 'hidden')
    hiddenField.setAttribute('name', key)
    hiddenField.setAttribute('value', message.data[key])
    form.appendChild(hiddenField)
  }
  document.body.appendChild(form)
  form.submit()
}

chrome.runtime.onMessage.addListener(onMessageHandler)
