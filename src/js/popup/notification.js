
export class notification {
  constructor (type, iconUrl, title, message) {
    this.type = type
    this.title = title
    this.iconUrl = iconUrl
    this.message = message
  }

  get message () {
    return this.message
  }

  set message (value) {
    this.message = value
  }
}
