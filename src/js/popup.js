import '../css/popup.css'
// import hello from './popup/example'
import { Mobizen } from './mobizen.js'
// import example from '../js/popup/example.js'
import $ from 'jquery'
// import Notification from './popup/notification.js'
// var URL = "http://127.0.0.1:6001";
const SUPPORT_URL = 'http://support-mirroring.mobizen.com/hc/ko/articles/'

var notifOptions = {
  type: 'basic',
  iconUrl: 'icon_48x48.png',
  title: 'Mobizen Extension',
  message: 'login()'
}

Mobizen.prototype.login = function (params, sessionLogin) {
  var linkTargetParam = {
    'targetUserEmail': ''
  }
  if (params.accounttype === 0) {
    linkTargetParam['targetUserEmail'] = params.userid
  } else {
    linkTargetParam['requestUserId'] = params.userid
    linkTargetParam['requestUserAccountType'] = params.accounttype
  }

  this.post(process.env.MOBIZEN_URL + '/account/linkTarget', linkTargetParam).done(function (res) {
    if (String(res.retcode) !== '200') {
      console.log('login errror - accountlist')
      sessionStorage.removeItem('authInfo')
      sessionStorage.removeItem('accessToken')
      sessionStorage.removeItem('refreshToken')
      sessionStorage.removeItem('device')
      sessionStorage.setItem('login', 'ok')
      location.href = '/'
      return
    }

    if (res.emailValid === false || res.accounts.length < 1) {
      sessionStorage.removeItem('authInfo')
      sessionStorage.removeItem('accessToken')
      sessionStorage.removeItem('refreshToken')
      sessionStorage.removeItem('device')
      notifOptions.message = '로그인을 실패하였습니다. \n 첫 연결은 메인 페이지에서 실행해 주세요.'
      chrome.notifications.clear('noti')
      chrome.notifications.create('noti', notifOptions)
      return
    }

    this.accounts = res.acounts

    console.log('login() method', res)
    this.authorize(params, sessionLogin)
  }.bind(this))
}

Mobizen.prototype.authorize = function (params, sessionLogin) {
  var reqParams = {
    userid: params.userid,
    token: params.token,
    accounttype: params.accounttype
  }

  console.log(params.token, 'token1')

  if (!sessionLogin) {
    console.log('!sessionLogin()')
  }

  this.post(process.env.MOBIZEN_URL + '/viewer/hostlist', reqParams).done(function (res) {
    if (res.retcode !== '200' && res.retcode !== '604') {
      // 세션스토리지에서 인증 삭제
      sessionStorage.removeItem('authInfo')
      sessionStorage.removeItem('accessToken')
      sessionStorage.removeItem('refreshToken')
      sessionStorage.removeItem('device')
    }

    // 계정이 없을때 // 비밀번호를 잘못쳐서 라이센스를 못받을때 // 임시코드 -> 백엔드 ret코드가 잘못오고 있음
    if (!res.userId || res.licenselist.length > 1) {
      sessionStorage.removeItem('authInfo')
      sessionStorage.removeItem('accessToken')
      sessionStorage.removeItem('refreshToken')
      sessionStorage.removeItem('device')
      notifOptions.message = '로그인을 실패하였습니다. \n 첫 연결은 메인 페이지에서 실행해 주세요.'
      chrome.notifications.clear('noti')
      chrome.notifications.create('noti', notifOptions)
      return
    }
    if (!mobizen.storage('login', 'session')) {
      notifOptions.message = '로그인을 성공하였습니다.'
      chrome.notifications.clear('noti')
      chrome.notifications.create('noti', notifOptions)
    }

    mobizen.storage('login', 'session', sessionLogin)
    $('#commonSignin').hide()
    $('#commonSelect').show()
    this.loginSuccess(params, sessionLogin, res)
  }.bind(this))
}

Mobizen.prototype.start = function (deviceIndex) {
  console.log('self start', 'selfstart()')

  var params = JSON.parse(sessionStorage.getItem('authInfo'))
  console.log(params)

  if (!params) {
    sessionStorage.removeItem('authInfo')
    sessionStorage.removeItem('accessToken')
    sessionStorage.removeItem('refreshToken')
    sessionStorage.removeItem('device')
    location.href = '/viewer/logout'
  }

  var reqParams = {
    token: params.token, // to code
    userid: params.userid,
    accounttype: params.accounttype
  }

  this.post(process.env.MOBIZEN_URL + '/viewer/hostlist', reqParams).done(function (res) {
    if (!res.userId) {
      sessionStorage.removeItem('authInfo')
      sessionStorage.removeItem('accessToken')
      sessionStorage.removeItem('refreshToken')
      sessionStorage.removeItem('device')
      location.href = '/viewer/logout'
    }
    this.startAction(deviceIndex)
  }.bind(this))
}

Mobizen.prototype.loginSuccess = function (params, sessionLogin, res) {
  var self = this
  let html = []
  let representDeviceName = null

  // $('#commonSignin').hide()
  // $('#commonSelect').show()

  for (let idx in res.hostlist) {
    let device = res.hostlist[idx]
    let checked = !idx ? 'checked' : ''
    let deviceName = (device.nickname || device.model).replace(/>/g, '&gt;').replace(/</g, '&lt;')
    if (!representDeviceName) {
      representDeviceName = deviceName
    }

    html.push([
      '<div class="device device-index-' + idx + '">',
      '<input type="radio" id="d' + idx + '" value="' + idx + '" name="device" ' + checked + '>',
      '<label for="d' + idx + '" title="' + deviceName + '" class="name">' + deviceName + '</label>',
      '<span class="delete" title="' + this.i18n('device.remove') + '"></span>',
      '</div>'
    ].join('\n'))
  }

  $('.devices').empty().html(html.join('\n'))

  this.options = {
    user: {
      token: res.token || params.token,
      userid: params.userid,
      type: params.accounttype,
      userIdToken: res.userId
    },
    debug: params.debug,
    hostlist: res.hostlist || [],
    licenselist: res.licenselist || [],
    displayname: params.displayname || ''
  }

  // Save Session storage
  params.token = res.token
  sessionStorage.setItem('authInfo', JSON.stringify(params))
  Mobizen.authInfo = params

  // 최종
  $('#commonSelect button.start-connect').on('click', function () {
    return self.start(0)
  })
}

Mobizen.prototype.startAction = function (deviceIndex) {
  var device = this.options.hostlist[+deviceIndex]
  function postData (url, data) {
    chrome.tabs.create(
      { url: chrome.runtime.getURL('post.html') },
      function (tab) {
        var handler = function (tabId, changeInfo) {
          if (tabId === tab.id && changeInfo.status === 'complete') {
            chrome.tabs.onUpdated.removeListener(handler)
            chrome.tabs.sendMessage(tabId, { url: url, data: data })
          }
        }

        // in case we're faster than page load (usually):
        chrome.tabs.onUpdated.addListener(handler)
        // just in case we're too late with the listener:
        chrome.tabs.sendMessage(tab.id, { url: url, data: data })
      }
    )
  }

  let payload = {
    'debug': this.options.debug,
    'viewer_userid': device.userid,
    'viewer_token': this.options.user.token,
    'viewer_accounttype': '0',
    'device_accounttype': device.accounttype,
    'device_availablenetworktype': '27',
    'device_devicekey': device.devicekey,
    'device_ip': device.ip,
    'device_model': device.model,
    'device_port': device.port,
    'device_userid': device.userid,
    'device_netstatus': device.netstatus,
    'use_connect_type': 'wireless'
  }

  postData(process.env.MOBIZEN_URL + '/home', payload)
}

var mobizen = new Mobizen()
$(function () {
  console.log('docuemnt ready')

  var $win = mobizen.$win
  var $body = mobizen.$body
  var $main = mobizen.$main
  var $signin = $('#signin')
  var $footer = $('.footer')
  var $devicelst = $('.device-list')
  var selectDevice = mobizen.$selectDevice

  var $username = mobizen.$signinform.find('#user-email')
  var $password = mobizen.$signinform.find('#user-pw')
  var $saveEmail = mobizen.$signinform.find('#save-email')
  var $debug = mobizen.$signinform.find('#debug')
  var storedEmail = mobizen.storage('login', 'email')

  try {
    storedEmail = decodeURIComponent(atob(mobizen.storage('login', 'email')))
  } catch (e) {
    mobizen.storage('login', 'email', '')
    storedEmail = ''
  }
  $username.val(storedEmail)

  $saveEmail.prop('checked', !(mobizen.storage('login', 'saveEmail') === false))
  mobizen.$signinform.on('click', 'label[for]', function (event) {
    var $target = $('#' + $(this).attr('for'))
    $target.prop('checked', !$target.prop('checked'))
    event.preventDefault()
  })

  // forgot password
  $signin.find('.forgot-password-link').on('click', function (event) {
    console.log('asdf')
    event.preventDefault()
    event.stopPropagation()
    window.open('https://www.mobizen.com/password/forget')
  })

  $signin.find('.signupbutton').on('click', function () {
    window.open('https://www.mobizen.com/')
  })

  $('#signin-button').click(function (e) {
    e.preventDefault()
    e.stopPropagation()

    chrome.notifications.onClosed.addListener(function (id, user) {
      console.log('closed')
    })

    chrome.notifications.onClicked.addListener(function () {
      console.log('clicked')
    })

    console.log('sign-button')
    authSubmit('global')
  })

  function postData (url, data) {
    chrome.tabs.create(
      { url: chrome.runtime.getURL('/post.html') },
      function (tab) {
        var handler = function (tabId, changeInfo) {
          if (tabId === tab.id && changeInfo.status === 'complete') {
            chrome.tabs.onUpdated.removeListener(handler)
            chrome.tabs.sendMessage(tabId, { url: url, data: data })
          }
        }
        // in case we're faster than page load (usually):
        chrome.tabs.onUpdated.addListener(handler)
        // just in case we're too late with the listener:
        chrome.tabs.sendMessage(tab.id, { url: url, data: data })
      }
    )
  }

  $('.logout').bind('click', function () {
    sessionStorage.removeItem('authInfo')
    sessionStorage.removeItem('accessToken')
    sessionStorage.removeItem('refreshToken')
    sessionStorage.removeItem('device')
    mobizen.storage('login', 'session', '')
    notifOptions.message = '모비즌 로그아웃'
    chrome.notifications.clear('noti')
    chrome.notifications.create('noti', notifOptions)
    location.href = '/popup.html'
  })

  $('.exit').bind('click', function () {
    window.close()
  })

  // tag a's _blank function not allow in chrome extension's CSP
  $footer.find('#slider a').on('click', function (event) {
    window.open(SUPPORT_URL + $(event.target).data('article'))
  })

  $('.main-page').on('click', function () {
    window.open('https://www.mobizen.com/')
  })

  $('.option').on('click', function () {
    window.open('/options.html')
  })

  function authSubmit (authType) {
    console.log('authSubmit')
    let username = $.trim($username.removeClass('error').val())
    let password = $.trim($password.removeClass('error').val())
    let debug = ($debug.length && $debug.prop('checked')) ? 1 : 0

    if (!username) $username.addClass('error').focus()
    if (!password) $password.addClass('error').focus()

    if (!username || !password) {
      notifOptions.message = '로그인을 실패하였습니다.'
      chrome.notifications.clear('noti')
      chrome.notifications.create('noti', notifOptions)
      return
    }

    let loginParam = {
      userid: username,
      token: password,
      accounttype: 0,
      devicetype: 1,
      debug: debug
    }

    mobizen.login(loginParam, loginParam)

    if ($saveEmail.prop('checked') && loginParam.accounttype == 0) {
      mobizen.storage('login', 'email', btoa(encodeURIComponent(username)))
      mobizen.storage('login', 'saveEmail', true)
    } else {
      mobizen.storage('login', 'email', '')
      mobizen.storage('login', 'saveEmail', false)
    }
  }

  function postData (url, data) {
    chrome.tabs.create(
      { url: chrome.runtime.getURL('post.html') },
      function (tab) {
        var handler = function (tabId, changeInfo) {
          if (tabId === tab.id && changeInfo.status === 'complete') {
            chrome.tabs.onUpdated.removeListener(handler)
            chrome.tabs.sendMessage(tabId, { url: url, data: data })
          }
        }

        // in case we're faster than page load (usually):
        chrome.tabs.onUpdated.addListener(handler)
        // just in case we're too late with the listener:
        chrome.tabs.sendMessage(tab.id, { url: url, data: data })
      }
    )
  }

  function sessionLogout () {
    sessionStorage.removeItem('authInfo')
    sessionStorage.removeItem('accessToken')
    sessionStorage.removeItem('refreshToken')
    sessionStorage.removeItem('device')
    mobizen.storage('login', 'session', '')
  }

  //  slider banner
  // var slideWidth = $('#slider ul li').width()
  var slideHeight = $('#slider ul li').height()

  function moveDown () {
    $('#slider ul').animate({
      top: -slideHeight
    }, 200, function () {
      $('#slider ul li:first-child').appendTo('#slider ul')
      $('#slider ul').css('top', '')
    })
  };

  setInterval(function () {
    moveDown()
  }, 1800)

  let storedsession = mobizen.storage('login', 'session')
  if (storedsession)
    {mobizen.authorize(storedsession)}
})
