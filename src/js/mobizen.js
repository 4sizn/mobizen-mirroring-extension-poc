/*!
 *
 * Mobizen 2 auth Base (Web based)
 *
 * IMPORTANT NOTE: This file is licensed only for use in providing the RSUPPORT services,
 *
 * @license Copyright (c) 2014 RSUPPORT Corp. (http://www.rsupport.com/)
 *
 * @version 2.9.1
 *
 * @require common/js/libs.js
 *
 */
import $ from 'jquery';

(function (exports, undefined) {
  'use strict';

  /**
     * @main Mobizen
     */
  function Mobizen () {
    var self = this
  
      this.$win = $(window)
      this.$body = $(document.body)
      this.$main = this.$body.children('main.content-wrap')
      this.$footer = this.$body.find('#footer')
      this.$languageSelector = $('#language')
      this.$scrollBody = this.$main
      this.windowHeight = this.$win.height()
      this.scrollTop = 0
      this.wasLazyLoad = false
      this.showFooterPosition = 150
      //signin vars
      this.$signinform = $('#signin-form')
      this.$selectDevice = $('#select-device')
      this.$message = this.$main.find('#signin-error-message')
      this.$sectionInner = $('#signin .section-inner')
      this.options = {}
  
      this.Platform = {
      isTablet: !!navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|Windows Phone)/),
      supportsTouch: 'ontouchstart' in window || !!navigator.msMaxTouchPoints
    }
  
      this.Platform.browser = (function () {
      var agent = navigator.userAgent,
           temp,
           match = agent.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [],
           browser
  
        if (/trident/i.test(match[1])) {
        temp = /\brv[ :]+(\d+(\.\d+)?)/g.exec(agent) || []
          browser = ['msie', temp[1]]
        } else {
        match = match[2] ? [match[1], match[2]] : [navigator.appName, navigator.appVersion, '-?']
          if ((temp = agent.match(/version\/([\.\d]+)/i)) != null) match[2] = temp[1]
          browser = match.join(' ').split(' ')
        }

      browser[0] = browser[0].toLowerCase()
        browser[1] = (browser[0] != 'safari')
                      ? parseInt(browser[1], 10)
                        : (isNaN(parseFloat(browser[2], 10)) == true)
                          ? parseFloat(browser[1], 10)
                          : parseFloat(browser[2], 10)
        return browser
      })()
  
      this.Platform.os = (function (browser) {
      var agent = navigator.userAgent,
           pattern = agent.match(/Windows NT|Mac OS X|Android/),
           osName, osVersion
  
        if (pattern && pattern.length) {
        pattern = pattern[0]
          osName = pattern.replace(/\s/g, '').replace('WindowsNT', 'windows').toLowerCase()
  
          if (osName === 'macosx' && agent.match(/Mobile/)) {
          osName = 'ios'
            pattern = 'CPU (iPhone )?OS'
          }
        osVersion = agent.match(new RegExp(pattern + '\\s?[\\d._]{0,};?'))[0].replace(/;$/, '').split(' ')
          osVersion = osVersion[osVersion.length - 1].replace(/[^\d_.]/g, '').replace(/_/g, '.')
        } else {
        osName = 'unknown'
        }
      return {
        name: osName,
        version: osVersion
      }
      })(this.Platform.browser[0])
  
      this.Platform.isSupportBrowser = (function (_this) {
      var ieVer = 11
        var name = _this.Platform.browser[0],
           version = _this.Platform.browser[1]        
        return (name.match(/chrome|firefox|safari|msie/) != null) ? (
        name == 'chrome' && version >= 21 ||
          name == 'firefox' && version >= 18 ||
          name == 'safari' && version >= 7.1 ||
          name == 'msie' && version >= ieVer
      ) : (support3d() && supportWSB())
      })(this)
  
      this.isAndoird = (/(Android).+Mobile/).test(navigator.userAgent)
  
      /* select language */
      var $languageList = this.$languageSelector.find('#language-list')
      var $languageButton = this.$languageSelector.find('#language-btn')
      this.$languageSelector.find('#select-language').on('click', function (event) {
      event.preventDefault()
        event.stopPropagation()
        var isVisible = $languageList.is(':visible')
        $languageList[isVisible ? 'hide' : 'show']()
        $languageButton[isVisible ? 'removeClass' : 'addClass']('open')
      })
      $languageButton.on('click', function (event) {
      event.stopPropagation()
      })
      $languageList.find('li').on('click', function () {
      event.preventDefault()
        event.stopPropagation()
  
        var search = String(location.search).replace(/[\?\&]?locale=[^\&]*/, '')
        var locale = $(this).find('a').attr('href').replace(/^\//, '')
        if (search) {
        locale = locale.replace(/^\?/, '&')
        }
      location.href = location.pathname + search + locale
      })
  
      this.$body.on('click', function (event) {
      $('.hostnone-box .guide-btn').removeClass('active')
        $languageList.hide()
        var popups = this.$body.find('.overlay').data('popups')
        if (popups && popups.length) {
        $('#' + popups[0]).find('.close').trigger('popupclose')
        }
    }.bind(this))
  
      this.$body.find('button.autohide').bind('click', function () {
      this.$body.trigger('click')
        var _id = $(this).attr('id')
        if (_id == 'gplus-join') {
        ga('send', 'event', 'login', 'web', 'create-google-account')
        } else if (_id == 'email-join') {
        ga('send', 'event', 'login', 'web', 'create-mail-account')
        }
    }.bind(this))
  
      /* App download */
      $('#app-download, #go-app-store a, #apk-update-alert-old .go-store, #footer .go-store').on('click', function (event) {
      event.stopPropagation()
        if (this.$body.find('#apk-update-alert-old:visible').length) {
        this.$body.click()
        }

      if (this.$body.find('#apk-update-alert:visible').length) {
        this.$body.click()
        }

      if (apkForSamsungDisplayed) {
        event.preventDefault()
          this.popup(this.$body.find('#app-download-popup'))
        } else {
        if (isConnectFromJP || !hasMarket) {
          ga('send', 'event', 'download', 'apkdown', 'login-login-apkdown-left')
          }
        else {
          ga('send', 'event', 'googleplay', 'outtogoogleplay', 'login-googleplay-outtogoogle-left')
          }
      }
    }.bind(this))
  
      /* Plugin download */
      $('#plugin-download, .plugin-download').on('click', function (event) {
      event.preventDefault()
        event.stopPropagation()
  
        self.popup(self.$body.find('#plugin-download-popup'))
        var src = $(this).attr('href')
  
        setTimeout(function () {
        var $downloadFrame = $('<iframe />', {
          name: 'pluginHidden',
          src: src
        }).css({'height': '0', 'width': '0', 'border': 'none'}).appendTo('body')
          setTimeout(function () { $downloadFrame.remove() }, 1500)
        }, 500)
      })
  
      /* about safe */
      this.$footer.find('.about-safe-browsing-link').on('click', function (event) {
      event.preventDefault()
        event.stopPropagation()
        self.popup(self.$body.find('#about-safe-browsing'))
      })
  
      $('.gnb-mobizen .prev').bind('click', function () {
      location.href = '/';
    })
    }

  // Mobizen.prototype.chkEvent = function(eventId, callback) {
  //   this.post('/eventExists/' + eventId).done(function(res) {
  //     callback(res);
  //   }.bind(this)).error(function() {
  //     this.message('Server: request error', 'error');
  //   }.bind(this));
  // };
  Mobizen.prototype.checkEvent = function (callback, rqdatas) {
    this.post('/noticeList', rqdatas).done(function (res) {
      callback(res)
      }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
    };

  Mobizen.prototype.bodyClickHandler = function () {
    this.$body.on('click', function (event) {
      this.$languageSelector.find('#language-list').hide()
        var popups = this.$body.find('.overlay').data('popups')
        if (popups && popups.length) {
        $('#' + popups[0]).find('.close').trigger('popupclose')
        }
    }.bind(this))
    }

  Mobizen.prototype.popupClose = function (sourdata) {
    var popups = this.$body.find('.overlay').data('popups')
      if (popups && popups.length) {
      $('#' + popups[0]).find('.close').trigger('popupclose')
      }
  }

  Mobizen.prototype.i18n = function (source, data) {
    if (source && !data) {
      try {
        var keys = source.split('.'), v = exports.message[keys.shift()]
          for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]]
          return v || '{' + source + '}'
        } catch (e) {
        return '{' + source + '}'
        }
    } else {
      if (!source.match(/\{([\w\.]*)\}/g)) source = this.i18n(source)
        return source.replace(/\{([\w\.]*)\}/g, function (str, key) {
        try {
          var keys = key.split('.'), v = data[keys.shift()]
            for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]]
            return (typeof v !== 'undefined' && v !== null) ? v : str
          } catch (e) {
          return str
          }
      })
      }
  }
  
    Mobizen.prototype.storage = function (name, key, val) {
    var storage = JSON.parse(localStorage[name] || '{}')
      //if (this.deviceKey) name += this.deviceKey;
      if (key && key.constructor == Object) {
      localStorage[name] = JSON.stringify(key)
      } else if (key === undefined) {
      return storage
      } else if (val === undefined) {
      return storage[key]
      } else {
      storage[key] = val
        if (this.Platform.os.name != 'ios') {
        localStorage[name] = JSON.stringify(storage)
        }
    }
  }
  
    Mobizen.prototype.popup = function (elem) {
    var $popupElem = $(elem),
         $overlay = this.$body.find('.overlay'),
         popups = $overlay.data('popups')
  
      if (!$popupElem.length) {
      return
      }
    if (popups === undefined) {
      popups = []
      }
    popups.unshift($popupElem.attr('id'))
  
      $overlay.data('popups', popups)
      .css('z-index', popups.length * 99)
      .addClass('show animate')
  
      //
  
      $popupElem.addClass('show animate')
      .css('z-index', popups.length * 100)
      .on('click', function (event) {
        event.stopPropagation()
        })
  
      this.lazyImageLoad($popupElem.find('img, .lazy-load'))
  
      var $close = $popupElem.find('a.close, button.btn-close'),
         isSupportBrowser = this.Platform.isSupportBrowser
  
      if ($close.length) {
      $close.one('click popupclose', function () {
        $close.off('click popupclose')
          popups = $overlay.data('popups')
          popups.shift()
          $overlay.data('popups', popups)
  
          if (popups.length == 0) {
          if (isSupportBrowser) {
            $overlay.removeClass('animate')
              .one('transitionend webkitTransitionEnd', function (event) {
                $overlay
                  .off('transitionend webkitTransitionEnd')
                  .removeClass('show')
                  .css('z-index', '-1')
                })
            } else {
            $overlay
              .removeClass('show animate')
              .css('z-index', '-1')
            }
        } else {
          $overlay.css('z-index', popups.length * 99)
          }

        $popupElem.removeClass('animate')
          .one('transitionend webkitTransitionEnd', function (event) {
            //
            $popupElem
              .off('transitionend webkitTransitionEnd')
              .removeClass('show')
              .css('z-index', '-1')
            })
        })
      }
  }
  
    Mobizen.prototype.lazyImageLoad = function (elems, imagePath) {
    var $elems = elems ? $(elems) : this.$body.find('.lazy-load')
      $elems.each(function (index) {
      var $el = $(this),
           path = imagePath || $el.data('src')
        if (path) {
        if ($el.is('img')) {
          !$el.attr('src') && $el.attr('src', path)
          } else {
          $el.css('background-image', 'url(' + path + ')')
          }
      }
    })
    };

  Mobizen.prototype.toggleFooterBg = function () {
    this.$footer.toggleClass('has-bg', this.windowHeight <= 920 || this.$scrollBody.scrollTop() > this.showFooterPosition)
    };

  Mobizen.prototype.emailChkUi = function (email, $label, $joinbutton) {
    if (email == '') {
      $label.html(this.i18n('account.text19'))
        $joinbutton.prop('disabled', true)
      } else if (email.match(/@/) == null) {
      $label.html(this.i18n('account.text92'))
        $joinbutton.prop('disabled', true)
      } else if (email.match(/^@.*/)) {
      $label.html(this.i18n('account.text93'))
        $joinbutton.prop('disabled', true)
      } else if (email.match(/.*@$/)) {
      $label.html(this.i18n('account.text94'))
        $joinbutton.prop('disabled', true)
      } else if (!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
      $label.html(this.i18n('account.text95'))
        $joinbutton.prop('disabled', true)
      } else {
      $label.html('')
        $joinbutton.prop('disabled', false)
      }
  }
  
    Mobizen.prototype.post = function (url, params, success) {
    var settings = {
      url: url,
      type: 'POST',
      data: JSON.stringify(params),
      dataType: 'json',
      contentType: 'application/json; charset=utf-8'
    }
      if (success) {
      settings.success = success
      }
    return $.ajax(settings)
    };

  Mobizen.prototype.get = function (url, params, success) {
    var settings = {
      url: url,
      type: 'GET',
      data: params,
      dataType: 'json',
      contentType: 'application/json; charset=utf-8'
    }
      if (success) {
      settings.success = success
      }
    return $.ajax(settings)
    };

  Mobizen.prototype.paymentHistory = function (params, callback) {
    this.post('/viewer/historylist', params).done(function (res) {
      callback(res)
      })
    };

  Mobizen.prototype.passwordChange = function (params, callback) {
    // 이메일 등록 및 수정
    this.post('/password/change', params).done(function (res) {
      callback(res)
      }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
    };

  Mobizen.prototype.accountRemove = function (options, removePass, callback) {
    var requestParam = {
      'userid': options.user.userid,
      'accounttype': options.user.type,
      'token': removePass
    }
      
      // 이메일 등록 및 수정
      this.post('/account/remove', requestParam).done(function (res) {
      callback(res)
      }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
  
    };

  Mobizen.prototype.hostLogout = function (accounttype, userid, devicekey, callback) {
    var requestParam = {
      'userid': userid,
      'accounttype': accounttype,
      'devicekey': devicekey
    }
      
      // 이메일 등록 및 수정
      this.post('/viewer/device_logout', requestParam).done(function (res) {
      callback(res)
      }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
  
    };

  /**
     * DEVICE 전체 삭제
     */
  Mobizen.prototype.deleteDeviceAll = function (params, callback) {
    this.post('/viewer/device_remove_all', {
      userid: this.sha256(params.userid),
      accounttype: params.accounttype
    }).done(function (res) {
      callback(res)
      })
  
    };

  Mobizen.prototype.resetPassword = function (email, resetPasswordType, callback) {
    // 비밀번호 변경 이메일 발송
    this.post('/account/reset_password', {'email': email, 'resetPasswordType': resetPasswordType}).done(function (res) {
      callback(res)
      }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
    };

  Mobizen.prototype.registerEmail = function (params, callback) {
    var email = (params.accounttype == 0) ? params.userid : params.displayEmail
  
      var requestParam = {
      'email': email,
      'userid': params.userid,
      'accounttype': params.accounttype
    }
      
      // 이메일 등록 및 수정
      this.post('/account/registerEmail', requestParam).done(function (res) {
      callback(res)
      }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
    };

  Mobizen.prototype.timeToString = function (timestamp) {
    // var timezone = Number((new Date().getTimezoneOffset()/60) * -1)*3600*1000;
    var localTime = new Date(Number(timestamp))
  
      localTime = [
      localTime.getFullYear(),
      (String(localTime.getMonth() + 1).length == 1) ? '0' + String(localTime.getMonth() + 1):String(localTime.getMonth() + 1),
      (String(localTime.getDate()).length == 1) ? '0' + String(localTime.getDate()):String(localTime.getDate())
    ].join('-')
  
      return localTime
    };

  Mobizen.prototype.accountUnion = function (params, sessionLogin, callback) {
    var email = (params.accounttype == 0) ? params.userid : params.displayEmail
  
      var requestParam = {
      'targetUserEmail': email,
      'requestUserId': params.userid,
      'requestUserAccountType': params.accounttype,
      'requestUserToken': params.token
    }
  
  
      // 계정 통합 중입니다 다이얼로그 띄우기
      //this.popup(this.$body.find('#unioning'));
  
      // 계정통합
      this.post('/account/link', requestParam).done(function (res) {
      // $("#unioning").find("a.close").trigger('click');

      if (String(res.retcode) != '200') {
        alert('login error - accountUnion')
          sessionStorage.removeItem('authInfo')
          sessionStorage.removeItem('accessToken')
          sessionStorage.removeItem('refreshToken')
          sessionStorage.removeItem('device')
          return;
      }

      if (callback) {
        callback()
        }
      /*
        else {
          // hostlist를 태워서 로그인 시킴
          this.realLogin(params, sessionLogin);
        }
        */
    }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
    };

  // Added Param for SimpleMode 2015-04-06
  Mobizen.prototype.message = function (msg, type, isSimpleMode) {
    var elName = (!isSimpleMode) ? '#signin-error' : '#simplemode-error',
      $errorMsg = this.$main.find(elName)
  
      if (this.messageTimeout) {
      clearTimeout(this.messageTimeout)
      }
    this.$message = this.$main.find(elName + '-message')
      this.$message.html(msg).parent()[msg ? 'show' : 'hide']().addClass(type)
  
      $errorMsg.css('margin-top', -$errorMsg.outerHeight() - 5)
  
      if ($errorMsg.is(':visible')) {
      $errorMsg.click(function () {
        $errorMsg.fadeOut()
        })
        this.messageTimeout = setTimeout(function () {
        $errorMsg.fadeOut()
        }, 27000)
      }
  }
  
    // 회원가입 시킴
    Mobizen.prototype.signup = function (params, callback) {
    var self = this
      var email = (params.accounttype == 0) ? params.userid : params.displayEmail
      var requestParam = {
      'userid': this.sha256(params.userid),
      'accounttype': params.accounttype,
      'password': (params.token) ? params.token : '',
      'email': email
    }

    this.post('/host/signup', requestParam).done(function (res) {
      callback(res)
  
      }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
    };

  // 회원가입 시킴 (소셜로 인증된 상태에서 이메일 인증없이 바로 가입 시키기)
  Mobizen.prototype.signupOfCross = function (requestParam, callback) {
    this.post('/host/signup', requestParam).done(function (res) {
      callback(res)
      }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
    };

  // 회원가입 상태 리턴
  Mobizen.prototype.signUpStatusChk = function (email, callback, failCallback) {
    this.post('/account/linkTarget', {'targetUserEmail': email}).done(function (res) {
      var accounts = res.accounts
        var signUpStatus = 'NONE'; // FINISHED or STARTED
      var code = res.retcode
  
        if (code === '413') {
        failCallback()
            return;
      }

      accounts.forEach(function (accountItem) {
        if (accountItem.accountType == 0) {
          signUpStatus = accountItem.signupStatus
            }
      })
        callback(signUpStatus)
      }).error(function () {
      console.log('linkTargetError')
      })
    };

  Mobizen.prototype.emailCertificatedChk = function (email, callback) {
    this.post('/account/linkTarget', {'targetUserEmail': email}).done(function (res) {
      var accounts = res.accounts
        var certificated = false
        accounts.forEach(function (accountItem) {
        if (accountItem.accountType == 0 && accountItem.passwordInitNeed === false) {
          certificated = true
          }
      })
        callback(certificated)
      }).error(function () {
      console.log('linkTargetError')
      })
    };

  Mobizen.prototype.existFacebookEmail = function (email, callback) {
    this.post('/account/linkTarget', {'targetUserEmail': email}).done(function (res) {
      if (String(res.retcode) !== '200') {
        alert('login error - accountlist')
          sessionStorage.removeItem('authInfo')
          sessionStorage.removeItem('accessToken')
          sessionStorage.removeItem('refreshToken')
          sessionStorage.removeItem('device')
          location.href = '/';
        return
        }

      var accounts = res.accounts
        var exist = false
        accounts.forEach(function (accountItem) {
        if (accountItem.accountType === 4) {
          exist = true
          }
      })
        if (res.emailValid === true) {
        exist = true
        }
      callback(exist)
      }).error(function () {
      sessionStorage.removeItem('authInfo')
        sessionStorage.removeItem('accessToken')
        sessionStorage.removeItem('refreshToken')
        sessionStorage.removeItem('device')
        alert('login error - accountlist')
        location.href = '/';
    })
    };

  Mobizen.prototype.loginChk = function (params, callback) {
    var authParam = {
      token: params.token, // to code
      userid: params.userid,
      accounttype: params.accounttype
      // devicetype: params.devicetype
    }
  
  
      if (params.captcha) {
      authParam.captcha = params.captcha
      }


    this.post('/viewer/hostlist', authParam).done(function (res) {
      callback(res)
      }).error(function () {
      this.message('Server: request error', 'error')
      }.bind(this))
    };

  Mobizen.prototype.checkToday = function (timestamp) {
    var targetDate = Number(this.timeToString(timestamp).replace(/-/g, ''))
      var nowDate = Number(this.timeToString((new Date()).getTime()).replace(/-/g, ''))
      if (targetDate === nowDate) {
      return true
      } else {
      return false
      }
  }
  
    Mobizen.prototype.checkEndDate = function (timestamp) {
    var start = timestamp - (60 * 60 * 24 * 1000)
      var end = timestamp
      var today = new Date().getTime()
  
      if (start <= today && end >= today) {
      return true
      } else {
      return false
      }
  }
  
    Mobizen.prototype.makeEventTerm = function (start, end) {
    var eventTerm = end - start
      return Math.floor(eventTerm / 86400000)
    };

  Mobizen.prototype.sha256 = function (s) {
    var chrsz = 8
      var hexcase = 0
      function safe_add (x, y) {
      var lsw = (x & 0xFFFF) + (y & 0xFFFF)
          var msw = (x >> 16) + (y >> 16) + (lsw >> 16)
          return (msw << 16) | (lsw & 0xFFFF)
      }
    function S (X, n) { return (X >>> n) | (X << (32 - n)) }
    function R (X, n) { return (X >>> n) }
    function Ch (x, y, z) { return ((x & y) ^ ((~x) & z)) }
    function Maj (x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)) }
    function Sigma0256 (x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)) }
    function Sigma1256 (x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)) }
    function Gamma0256 (x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)) }
    function Gamma1256 (x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)) }
    function core_sha256 (m, l) {
      var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1,
        0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
        0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786,
        0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
        0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147,
        0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
        0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B,
        0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
        0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A,
        0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
        0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2)
          var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19)
          var W = new Array(64)
          var a, b, c, d, e, f, g, h, i, j
          var T1, T2
          m[l >> 5] |= 0x80 << (24 - l % 32)
          m[((l + 64 >> 9) << 4) + 15] = l
          for (var i = 0; i < m.length; i += 16) {
        a = HASH[0]
              b = HASH[1]
              c = HASH[2]
              d = HASH[3]
              e = HASH[4]
              f = HASH[5]
              g = HASH[6]
              h = HASH[7]
              for (var j = 0; j < 64; j++) {
          if (j < 16) W[j] = m[j + i]
                  else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16])
    
                  T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j])
                  T2 = safe_add(Sigma0256(a), Maj(a, b, c))
    
                  h = g
                  g = f
                  f = e
                  e = safe_add(d, T1)
                  d = c
                  c = b
                  b = a
                  a = safe_add(T1, T2)
              }
        HASH[0] = safe_add(a, HASH[0])
              HASH[1] = safe_add(b, HASH[1])
              HASH[2] = safe_add(c, HASH[2])
              HASH[3] = safe_add(d, HASH[3])
              HASH[4] = safe_add(e, HASH[4])
              HASH[5] = safe_add(f, HASH[5])
              HASH[6] = safe_add(g, HASH[6])
              HASH[7] = safe_add(h, HASH[7])
          }
      return HASH
      }
    function str2binb (str) {
      var bin = Array()
          var mask = (1 << chrsz) - 1
          for (var i = 0; i < str.length * chrsz; i += chrsz) {
        bin[i >> 5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i % 32)
          }
      return bin
      }
    function Utf8Encode (string) {
      string = string.replace(/\r\n/g, '\n')
          var utftext = '';
      for (var n = 0; n < string.length; n++) {
        var c = string.charCodeAt(n)
    
              if (c < 128) {
          utftext += String.fromCharCode(c)
              } else if ((c > 127) && (c < 2048)) {
          utftext += String.fromCharCode((c >> 6) | 192)
                  utftext += String.fromCharCode((c & 63) | 128)
              } else {
          utftext += String.fromCharCode((c >> 12) | 224)
                  utftext += String.fromCharCode(((c >> 6) & 63) | 128)
                  utftext += String.fromCharCode((c & 63) | 128)
              }
      }
      return utftext
      }
    function binb2hex (binarray) {
      var hex_tab = hexcase ? '0123456789ABCDEF' : '0123456789abcdef';
      var str = '';
      for (var i = 0; i < binarray.length * 4; i++) {
        str += hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8 + 4)) & 0xF) +
              hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8 )) & 0xF)
          }
      return str
      }
    s = Utf8Encode(s)
      return binb2hex(core_sha256(str2binb(s), s.length * chrsz))
    };

  Mobizen.prototype.parseJwt = function (token) {
    var base64Url = token.split('.')[1]
        var base64 = base64Url.replace('-', '+').replace('_', '/')
        return JSON.parse(window.atob(base64))
    };

  Mobizen.prototype.getRefreshToken = function () {
    $.ajax({
      type: 'post',
      url: '/account/refreshToken',
      data: JSON.stringify({'refresh_token': sessionStorage.getItem('refreshToken')}),
      dataType: 'json',
      contentType: 'application/json',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + sessionStorage.getItem('accessToken'))
            },
      success: function (res) {
        if (res.retcode === '200') {
          sessionStorage.setItem('accessToken', res.access_token)
                    sessionStorage.setItem('refreshToken', res.refresh_token)
                } else {
          console.log(res.retcode, '다시 로그인 필요')
                    location.href = '/viewer/logout';
        }
      }
    })
    };

  exports.Mobizen = Mobizen
  })(this)

