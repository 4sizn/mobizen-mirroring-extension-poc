import '../img/icon-128.png'
import '../img/icon-34.png'
import '../img/icon_16x16.png'
import '../img/icon_48x48.png'
import '../img/icon_128x128.png'
import $ from 'jquery'

/* global io */
log('background()')
var socket = io.connect(process.env.PUSH_SERVER_URL)
var username
var connected = false
var typing = false

socket.emit('add user', 'user1')

socket.on('user joined', function (data) {
  console.log('asdfasdf')
  log(data.username + ' joined')
  addParticipantsMessage(data)
})

socket.on('login', function (data) {
  connected = true
  // Display the welcome message
  var message = 'Welcome to Socket.IO Chat – '
  log(message)
  addParticipantsMessage(data)
})

function addParticipantsMessage(data) {
  var message = ''
  if (data.numUsers === 1) {
    message += "there's 1 participant"
  } else {
    message += 'there are ' + data.numUsers + ' participants'
  }
  log(message)
}

socket.on('new message', function (data) {
  log(data.message)
})

function log(msg) {
  var notifOptions = {
    type: 'basic',
    iconUrl: 'icon_48x48.png',
    title: 'Mobizen Extension',
    message: msg
  }

  chrome.notifications.clear('noti4')
  chrome.notifications.create('noti4', notifOptions)
}
